#include <iostream>
using namespace std;

class Investment                    // Case Sensitive Correction "class"
 {
    private:  //properties          // Case Sensitive Correction "private"
     
         long double value;
         float interest_rate = 0.083;

    public:  //methods              // Case Sensitive Correction And Spelling Error "public"
        
          void setVal(float val) // Set Method Takes Parameters And Is Void For Not Returning Values
            {
                value = val;
                int years = 0;
                
                do
                {
                    for (int i = 0; i < 12; i++)
                    {
                        value += 250.00;
                    }
                    
                    value += value*interest_rate;
                    years++;
                    
                } while (value < 1000000.00);
                
                cout << "It Will Take: " << years << " Years." << endl;
            }
     
          float getVal() // Get Method Takes No Parameters And Is Type Float For Returning Values.
            {
                return value;
            }
     
 };     // Semi-Colon Required At The Ending Of Class


/*
 
 All Corrections Include:
 - Case Sensitive Names For "class", "private", and "public"
 - Spelling Error In "public"
 - Semi-Colon Added At The End Of The Class
 - Proper Names For Set And Get Functions
 - Set Method Made Void And Accepting Parameter
 - Get Method Returns Float But Accepts Not Parameter
 */
